#ifndef SUPP_H
#define SUPP_H
#include <iostream>
#include <fstream>
#include "DLLNode.h"
void getFileStream(std::ifstream& stream, char* fileName);
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>*
    node2, Node<int>* node3);
void setPtrs(Node<int>*, Node<int>*, Node<int>*);
void printPtrs(std::ostream&, Node<int>*);
#endif  