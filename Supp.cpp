
/* File: Supp.h
 * Author: Bo Ni, Reilley Knott, Imari McKinney, Jorge Garcia
 * Email: bni@nd.edu, rknott@nd.edu,imckinne@nd.edu, rknott@nd.edu
 * The file that contains the prototype for the getFileStream function
**********************************************/

#include "Supp.h"
#include "DLLNode.h"

/********************************************
* Function Name  : getFileStream
* Pre-conditions : std::ifstream& stream, char* fileName
* Post-conditions: none
* The function that will open the input stream
********************************************/

void getFileStream(std::ifstream& inFile, char* fileName){
    // Create ifstream and open the stream with the file
    inFile.open(fileName);

    // Check if the file is open.
    if (!inFile.is_open()){
        std::cerr << "File Not Found! Exiting program..." << std::endl;
        exit(-1);
    }
}

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, Node<int>* node1, Node<int>*
    node2, Node<int>* node3
* Post-conditions: none
* The function that add Nodes
********************************************/
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>*
    node2, Node<int>* node3){
    inputStream >> node1->data;
    inputStream >> node2->data;
    inputStream >> node3->data;
}

/********************************************
* Function Name  : setPtrs
* Pre-conditions : Node<int>* node, Node<int>* prev, Node<int>* next
* Post-conditions: none
* The function that sets the pointer for a node
********************************************/
void setPtrs(Node<int>* node, Node<int>* prev, Node<int>* next){
    node->prev = prev;
    node->next = next;
}

/********************************************
* Function Name  : printPtrs
* Pre-conditions : std::ostream& out, Node<int>* node
* Post-conditions: none
* The function that prints a node's data, with its previous node's data and its next node's data
********************************************/
void printPtrs(std::ostream& out, Node<int>* node){
    if(node->prev != nullptr){
        out << "Previous: " << node->prev->data << std::endl;
    }
    if(node != nullptr){
        out << "Current: " << node->data << std::endl;
    }
    if(node->next != nullptr){
        out << "Next: " << node->next->data << std::endl;
    }
    out << std::endl;
}