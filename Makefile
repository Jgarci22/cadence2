C++ = g++
CFLAGS = -g -std=gnu++11 
C2P01MSG = $(info ********COMPILING CADENCE 2 - PART 01***********)
C2P01 = C2P01
SUPP = Supp
C2P01Comp = $(C++) $(CFLAGS) $(C2P01).cpp $(SUPP).cpp -o $(C2P01)
P01:
	$(C2P01MSG)
	$(C2P01Comp)

########################### Command: make P02 ###############################
# First, create a message to send to the user
C2P02MSG = $(info ********COMPILING CADENCE 2 - PART 2***********)
# Next, build the build target executable:
C2P02 = C2P02
SUPP = Supp
# Next, indicate the compilation commands
# Put together, this becomes: g++ -g -std=gnu++11 -Wpedantic
C2P02Comp = $(C++) $(CFLAGS) $(C2P02).cpp $(SUPP).cpp -o $(C2P02)
# Command: make P02
P02:
	$(C2P02MSG)
	$(C2P02Comp)

######################### Command: make all ###############################
# First, create a message to the user that this is for both versions
allMsg = $(info ********COMPILING All Problems************)
# Command: make all
# Output: ********COMPILING BOTH VERSIONS WITH make all************
# Output: g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr
all:
	$(allMsg)
	$(C2P01Comp)
	$(C2P02Comp)